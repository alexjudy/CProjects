#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>

#define MAXKEYWORD 256
#define MAXDIRPATH 1024
#define MAXOUTSIZE 2048
#define MAXLINESIZE 1024

#define MESSAGESIZE 1281
struct server_message
{
  long m_type;
  char m_content[MESSAGESIZE];
};
	
void send_message(char *line)
{
	int mq_id;
	key_t key;
	
	if((key = ftok("mqks_server.c", 1)) == -1)
	{
		perror("ftok");
		exit(-1);
	}

	if((mq_id = msgget(key, 0644)) == -1)
	{
		perror("msgget");
		exit(-1);
	}
	//for testing
	struct server_message sample_mess;
	sample_mess.m_type = 1;
  	strcpy(sample_mess.m_content, line);
  	if(msgsnd(mq_id, &sample_mess, MAXLINESIZE, 0) == -1) {
    	perror("Error in msgsnd");
  	}	
}
	
void open_file(char *user_file)
{
	FILE *file;	
	file = fopen(user_file, "r");
		
	if(file == NULL)
	{
		printf("%s could not be opened", user_file);
		exit(-1);		
	}
	char line[MAXLINESIZE];
	while(fgets(line, MAXLINESIZE, file) != NULL)
	{
		strtok(line, "\n"); //fgets includes the newline so trim it off here
		send_message(line);
	}
}

int main(int argc, char *argv[])
{
	if(argc != 2)
	{
		printf("This program requires at least one input file to be entered.");
		exit(-1);
	}
	open_file(argv[1]);
	return 0;
}
