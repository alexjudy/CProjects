// Advanced Calculator
// Author: Alex Judy
// Date: 2/4/2014
// Section: 1

#include <stdio.h>

    void addition(float, float);        //Void these inputs so that the program does not recognize them as a value
    void subtraction(float, float);
    void multiplication(float, float);
    void division(float, float);
    void modulus(int, int);
    void prime(int);
    void factorial(int);
    void power(int, int);
	void fibonacci(int);
	
int main()
{
printf("\n\nAdvanced Calculator"); //Program Title
int userInput=0, exit = 0, num3 = 0, num4 = 0; //Default values for first number and second number that will be used for whole numbers. Exit will always = 0
float num1 = 0, num2 = 0; //Default values for float numbers.
 {
  //Calculator function selection menu
  printf("\n -----------------------\n");
  printf("|(1) Addition           |\n");
  printf("|(2) Subtraction        |\n");
  printf("|(3) Multiplication     |\n");
  printf("|(4) Division           |\n");
  printf("|(5) Modulus            |\n");
  printf("|(6) Test if Prime      |\n");
  printf("|(7) Factorial          |\n");
  printf("|(8) Power              |\n");
  printf("|(9) Fibonacci Sequence |\n");
  printf("|(0) Exit Calculator    |\n");
  printf(" -----------------------\n");
  printf("\nPlease Select Operation: \n"); //Select Operation

   {scanf("%d", &userInput); //User operation choice
    switch(userInput)

    { case 1: case 2: case 3: case 4: //Options 1-4
      printf("Enter your first number: \n");
		scanf("%f" , &num1);
      printf("Enter your second number: \n");
		scanf("%f" , &num2);
		switch(userInput)
		
		
	   { case 1: //Option 1 for addition 
		addition (num1, num2);
		break;
		
		 case 2: //Option 2 for subtraction
		subtraction (num1, num2);
	 	break;
		 
		 case 3: //Option 3 for multiplication
		multiplication (num1, num2);
		break;
		
		 case 4: //Option 4 for division
		division (num1, num2);
		break;
       }
        return main();
		break;
       
     case 6: case 7: case 9: //Options 6,7 & 9
       	printf("Enter Number: \n");
       	scanf("%d", &num3);
       	switch(userInput)
       	
       {
         case 6: //Option 6 for prime
	   	prime (num3);
       	break;
       	
       	 case 7: //Option 7 for factorials 
       	factorial (num3); 	
        break;
         
		 case 9: //Option 9 for a fibonacci sequence
        fibonacci (num3);
		break; 	
	   }
	   return main();
	    break;
	    
	 case 5: case 8:
	    printf("Enter Number: \n");
		scanf("%d", &num3);
		printf("Enter Number: \n");
		scanf("%d", &num4); 
		switch(userInput)
	   {
	   	 case 5: //Option 5 for modulus
	    modulus (num3, num4);	
	   	break;
	   	 
	   	 case 8: //Option 8 for a power
	   	power (num3, num4); 	
	   	break;
	   }
	   return main();	  
        break;
	    
	     case 0: //Exit option
        printf("Good Bye!\n");
        return 0;
        return main();
        break;
	   
	   
	    default:	//If a number less than 0 and greater than 9 is entered it is invalid
	     printf("Incorrect input. Please enter a valid input.");
        return main();
		 break;

    } //End calculator functions
   }//End user input for operations and switch
 } //End calculator 
system("pause");
} //End Main


//Definines what each user input operation does
       void addition(float n1, float n2) { //Function for addition
            printf("%f + %f = %f", n1, n2, n1 + n2);
            }
            
       void subtraction(float n1, float n2) { //Function for subtraction
            printf("%f - %f = %f", n1, n2, n1 - n2);
            }
            
       void multiplication(float n1, float n2) { //Function for multplication
            printf("%f * %f = %f", n1, n2, n1 * n2);
            }
            
       void division(float n1, float n2) { //Function for division
            printf("%f / %f = %f", n1, n2, n1 / n2);
            }
            
       void modulus(int n1, int n2) { //Function to show the remainder after division
            printf("%d %% %d = %d", n1, n2, n1 % n2);
        }
            
       void prime(int n) {
            int prime = 1, test = 2; // Use default variables for test
            while(prime && test < n && n > 1)  // While a number is still known to be prime, greater than one and the test number is lower than it:
                            {
                             if ((n % test) == 0) // //Check for prime
                             prime = 0; // Prime = 0 which exits loop
                              else
                             test += 1; // Increase number and test as a factor
                            }
                               if (!prime){ // If not prime:
                               printf("Not prime: %d * %d = %d", test, (n / test), n); //If not prime then print factors of number
                                          }
                                             else {
                                             printf("Prime!"); //If prime then print prime
                                                  }
                        }
      
	   void factorial(int n) { //Function for finding factorials
             int i, result = 1;
             for(i = n; i > 0; i--){
                   result *= i;
             }
             printf("%d = %d", n, result);
         }
        
       void power(int n1, int n2){ //Function to add a power to a number
             int i, result = 1;
             for(i = n2; i > 0; i--){
                   result *= n1;
                   }
             printf("%d ^ %d = %d", n1, n2, result);
             }
             
      void fibonacci(int n) { //Function to create a fibonacci sequence 
             int iresults[n], i;
             iresults[0] = 0;
             iresults[1] = 1;
             for(i = 2; i < n; i++){
                      iresults[i] = iresults[i-1] + iresults[i-2];
                      }
             for(i = 0; i < n; i++){
                   printf("%d ", iresults[i]);
                   }          
             }
