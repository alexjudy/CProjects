#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/wait.h>
#include <dirent.h>
#include <semaphore.h>
#include <fcntl.h>

#define MAXDIRPATH 1024
#define MAXKEYWORD 256
#define MAXLINESIZE 1024
#define MAXOUTSIZE 2048
#define MESSAGESIZE 1281

struct server_message
{
  long m_type;
  char m_content[MESSAGESIZE];
};

typedef struct
{
	char dir_path[MAXDIRPATH];
	char filename[100];
	char keyword[MAXKEYWORD];
}searcher_struct;

typedef struct
{
	char filename[100]; //can resize as needed
	int line_num;
	char line[MAXLINESIZE];
}item;

/************/
//global queue
/************/
item *queue;
int queue_count = 0;
int max_buffer_size; //max size of the buffer
int add = 0; //queue location to add items
int removed = 0; //queue location to remove items
sem_t avail; //number of open locations
sem_t filled; //number of filled locations
sem_t mutex; //mutex

//keep track of the number of child processes
int children = 0;

//number of worker threads, used to determine when to end printer thread
int worker_count = 0;
int buffer_count = 0;
int printer_running = 0;

int find_keyword_in_line(char *line, char *keyword)
{
	int occurences = 0;
	char *save;
	char *splitstring = strtok_r(line, " ", &save);
	while(splitstring)
	{
		if(strcmp(splitstring, keyword) == 0)
			occurences++;
      		splitstring = strtok_r(NULL, " ", &save);
	}
	return occurences;
}

void add_item_to_buffer(char *filename, int line_num, char *line)
{
	sem_wait(&avail);
    sem_wait(&mutex);
    //critical section
    strcpy(queue[add].filename, filename);
    queue[add].line_num = line_num;
    strcpy(queue[add].line, line);
    add++;
    if(add == max_buffer_size)
    	add = 0;
    buffer_count++;
    //end of critical section
    sem_post(&mutex);
    sem_post(&filled);
}

void *worker(void *param)
{
//	printf("Entered worker thread\n");
	searcher_struct *args = param;	
	char *filename = args->dir_path;
	strcat(filename, "/");
	strcat(filename, args->filename);
	
	FILE * file = NULL;
	file = fopen(filename, "r");
	if (file == NULL)
	{
		printf("Could not open search file %s.\n", filename);
		printf("Errno is: %s\n", strerror(errno));
		pthread_exit(0);
	}
	char *keyword = args->keyword;
	
	char line[MAXLINESIZE];
	char line_copy[MAXLINESIZE];
	int line_num = 1;
	int count;
	char *save;
	while(fgets(line, MAXLINESIZE, file) != NULL)
	{
		count = 0;
		strcpy(line_copy, line); //since strtok in find_keyword_in_line modifies the original string
		strtok_r(line, "\n", &save);
		count = find_keyword_in_line(line, keyword);
		if(count)
		{
			//found the keyword
			//create an item for it
			//item consists of: file name:line number: full line
			
			//for testing:
			strtok(line_copy, "\n"); //since fgets keeps the newline, trim it off here
			add_item_to_buffer(args->filename, line_num, line_copy);
		}
		line_num++;
	}
	fclose(file);
	worker_count--;
	pthread_exit(0);
}

void *printer(void *param)
{
	int count = 0;
	item queue_item;
	while(1)
	{	
		if(count == buffer_count)
			break;
		//handle getting struct from buffer here
		sem_wait(&filled);
    	sem_wait(&mutex);
    	//critical section
    	queue_item = queue[removed];
    	removed++;
    	if(removed == max_buffer_size)
    		removed = 0;
    	//end of critical section
    	sem_post(&mutex);
    	sem_post(&avail);
    	
    	count++;
		//handle file locking here
		struct flock file_lock = {F_WRLCK, SEEK_END, 0, 0, 0};
		int fresult;
		fresult = open("output.txt", O_CREAT | O_APPEND | O_RDWR, 0666);
  		if (fresult == -1) {
    	    perror("open");
    	    pthread_exit(0);
  		}
  		file_lock.l_pid = getpid();
    	if (fcntl(fresult, F_SETLKW, &file_lock) == -1) 
    	{
    	    perror("fcntl");
    	    pthread_exit(0);
    	}
    
    	char *out_str = malloc(MAXOUTSIZE);
    	sprintf(out_str, "%s:%d:%s\n", queue_item.filename, queue_item.line_num, queue_item.line);
		write(fresult, out_str, strlen(out_str));
		free(out_str);
		file_lock.l_type = F_UNLCK;
		close(fresult);
	}
	printer_running = 0;
	pthread_exit(0);
}

void get_dir_files(char *directory, char *keyword)
{
	DIR *d;
	d = opendir(directory); //Put message path between quotes
	struct dirent *dir;
	
	int count = 0;
	
	if(d)
	{
		while ((dir = readdir(d)) != NULL)
		{
			//search the directory for files
			if(!strcmp(dir->d_name, ".") || !strcmp(dir->d_name, ".."))
				continue;
			count++;
		
			//found a valid file, create a thread for it
			searcher_struct *args = malloc(sizeof *args);
			strcpy(args->dir_path, directory);
			strcpy(args->filename, dir->d_name);
  			strcpy(args->keyword, keyword);
  			pthread_t tid;
  			pthread_attr_t attr;
  			pthread_attr_init(&attr);
  			pthread_create(&tid, &attr, worker, args);
  			worker_count++;
		}
		closedir(d);
		while(worker_count > 0)
		{
			//do nothing
			;
		}
	
		//create a printer thread for this directory
		item *param = malloc(sizeof *param);
		pthread_t tid_printer;
  		pthread_attr_t attr_printer;
  		pthread_attr_init(&attr_printer);
  		pthread_create(&tid_printer, &attr_printer, printer, param);
  		printer_running = 1;
        while(printer_running)
		{
			//do nothing
			;
		}
		free(param);
	}
	else
	{
		printf("Can't open directory %s\n", directory);
	}
}

int create_search(char *directory, char *keyword)
{
	children++;
	pid_t process_id;
    process_id = fork();
    if (process_id < 0) 
    {
    	printf("Failed to create child process\n");
    	return 0;
  	}
  	else if (process_id == 0) 
  	{
  		
        get_dir_files(directory, keyword);
  		return process_id;
    }
    else
    {
    	//parent process, probably should add some handling here
    	wait(NULL);
		return 1;
    }
}


int main(int argc, char *argv[])
{
	//check if all command line arguments are supplied
	if(argc < 2)
	{
		printf("Usage: ./mqks_server <buffersize>\n");
		return -1;
	}
	int buffer_size = atoi(argv[1]);
	//to shut up compiler warnings
	buffer_size += 0;
	max_buffer_size = buffer_size;
	//initialize our semaphores
	sem_init(&avail, 0, max_buffer_size); // all are empty 
  	sem_init(&filled, 0, 0);    		 // 0 are full
  	sem_init(&mutex, 0, 1);
	
	//create the item buffer
	queue = (item*)malloc(buffer_size*sizeof(item));
	
	//attempt to create a key for the queue
	key_t server_key;
	server_key = ftok("mqks_server.c", 1);
	if (server_key == -1)
	{
    	printf("Error in creating server_key\n");
    	return -1;
  	}
  	
  	//attempt to create the queue
  	int smqi; //server message queue id
  	smqi = msgget(server_key, 0644 | IPC_CREAT);
  	if (smqi == -1)
  	{
    	printf("Error in creating server message queue\n");
    	return -1;
    }
    
    //try to pull a message from the queue
    int mess_rec;
    struct server_message server_mess;
    char *directory;
    char *keyword;
    char *splitstring;
    int child_id = 0;
    while(1)
    {
    	mess_rec = msgrcv(smqi, &server_mess, MESSAGESIZE, 0, 0);
    	if (mess_rec == -1) 
    	{
    		printf("Error in receiving server message\n");
    		return -1;
    	}
    	
    	if(strcmp(server_mess.m_content, "exit") == 0)
    		break;
    	
    	//get the directory and keyword from the queue
    	splitstring = strtok(server_mess.m_content, " ");
    	directory = splitstring;
    	splitstring = strtok(NULL, " ");
    	keyword = splitstring;
    	//create a new process for this message
    	child_id = create_search(directory, keyword);
		if(child_id == 0)
			return 0;
		else
		{
			children--;
		}
    }
    while(children > 0)
    {
    	sleep(1);
    }
    if (msgctl(smqi, IPC_RMID, NULL) == -1)
    {
    	perror("msgctl");
    	exit(1);
  	}
    free(queue);
	return 0;
}
