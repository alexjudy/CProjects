// Advanced Phonebook
// Author: Alex Judy
// Date: 2/23/2014
// Section: 1

    #include <stdio.h>
    #include <stdlib.h>
    
    void addUser();
    void removeUser();
    void displayUser();
    void sort();
    void find();
    void displayRandom();
    void deleteAll();
    
    typedef struct {
            char *firstName;
            char *lastName;
            char *phoneNumber;
            } phoneBook;
            
    phoneBook *pb;
    
    int count = 0;
    
    
    main() {
           int userValue = 0;
           while (userValue != 8) {
                 printf("Phone Book Application\n");
                 printf("(1) Add friend\n");
                 printf("(2) Remove friend\n");
                 printf("(3) Show phone book\n");
                 printf("(4) Sort by First/Last Name\n");
                 printf("(5) Find a Friend\n");
                 printf("(6) Display a Random Friend\n");
                 printf("(7) Delete All Records\n");
                 printf("(8) Exit\n\n");
                 
                 printf("What do you want to do? ");
                 scanf("%d", &userValue);
                 printf("\n");
                 
           switch (userValue) {
                  case 1:
                       addUser();
                       break;
                  case 2:
                       removeUser();
                       break;
                  case 3:
                       displayUser();
                       break;
                  case 4:
                       sort();
                       break;
                  case 5:
                       find();
                       break;
                  case 6:
                       displayRandom();
                       break;
                  case 7:
                       deleteAll();
                       break;
                  case 8:
                       printf("Have a nice day!\n");
                       break;
                 }
                  
           }
           
           pb = NULL;
           free(pb);     
           system("pause");
       }

    void addUser() {
         if (count == 0) {
            pb = (phoneBook *) malloc(sizeof(phoneBook) + (count*40) + 40);
         } else {
            pb = (phoneBook *) realloc(pb, sizeof(phoneBook) + (count*40) + 40);
             }
             
         if (pb == NULL) {
                printf("You cannot add more friends because: OUT OF MEMORY\n");
         } else {
             pb[count].firstName = (char *) malloc(sizeof(char) * 15);
             pb[count].lastName = (char *) malloc(sizeof(char) * 15);
             pb[count].phoneNumber = (char *) malloc(sizeof(char) * 10);
             printf("First name: ");
             scanf("%s", pb[count].firstName);
             printf("Last name: ");
             scanf("%s", pb[count].lastName);
             printf("Phone Number: ");
             scanf("%s", pb[count].phoneNumber);
             
             count++;
             printf("\nRecord added to the phone book!\n\n");
         }
    }
    
    void removeUser() {
         char *fName = (char *) malloc(sizeof(char) * 15);;
         char *lName = (char *) malloc(sizeof(char) * 15);;
         int i, j;
         int isRemoved = 0;
         
         printf("First name: ");
         scanf("%s", fName);
         printf("Last name: ");
         scanf("%s", lName);
         
         for (i = 0; i < count; i++) {
             if ((strcmp(pb[i].firstName, fName) == 0) && (strcmp(pb[i].lastName, lName) == 0)) {
                for (j = i; j < count; j++) {
                    pb[j].firstName = pb[j+1].firstName;
                    pb[j].lastName = pb[j+1].lastName;
                    pb[j].phoneNumber = pb[j+1].phoneNumber;
                    }
                count -= 1;
                isRemoved = 1;
                break;                     
             } 
         }
         
         if (isRemoved == 1) {
            printf("\nRecord deleted from the phone book.\n\n");
         } else {
            printf("\nRecord not Found.\n\n");
         }
    }
    
    void displayUser() {
         int i;
         int printed = 0;
         
         printf("Phone Book: \n");
         for (i = 0; i < count; i++) {
                printf("\n(%d)\t%s\t%s\t%s\n", printed + 1, pb[i].firstName, pb[i].lastName, pb[i].phoneNumber);
                printed++;
         }
         printf("\n");
         if (!printed) {
            printf("No entries found!\n\n");
            }
    }
    
    void sort() {
         int j, x;
         phoneBook *temp;
         temp = (phoneBook *) malloc(sizeof(phoneBook) + 40);
         int type = 0;
         system("cls");
         if (count == 0){
                   printf("The phonebook is empty.\n");
                   system("pause");
         } else {
             printf("Sort by: \n0 - First Name \n1 - Last Name\nPlease enter a choice: ");
             scanf("%d", &type);
             printf("\n");
             
             switch(type) {
                          case 0:
                               for(j = 0; j < count; j++) {
                                      for (x = 0; x < count; x++) {
                                          if(strcmp(pb[j].firstName, pb[x].firstName) < 0) {
                                                    temp[0] = pb[j];
                                                    pb[j] = pb[x];
                                                    pb[x] = temp[0];
                                                    }
                                          }
                                      }                 
                               break;
                          case 1:
                               for(j = 0; j < count; j++) {
                                      for (x = 0; x < count; x++) {
                                          if(strcmp(pb[j].lastName, pb[x].lastName) < 0) {
                                                    temp[0] = pb[j];
                                                    pb[j] = pb[x];
                                                    pb[x] = temp[0];
                                                    }
                                          }
                                      }
                               break;
             }
         }
         
    }
         
    void find() {
         system("cls");
         if (count == 0){
                   printf("The phonebook is empty.\n");
                   system("pause");
         } else {
         
             char *fName = (char *) malloc(sizeof(char) * 15);;
             char *lName = (char *) malloc(sizeof(char) * 15);;
             int i;
             int isFound = 0;
             
             printf("First name: ");
             scanf("%s", fName);
             printf("Last name: ");
             scanf("%s", lName);
             
             for (i = 0; i < count; i++) {
                 if ((strcmp(pb[i].firstName, fName) == 0) && (strcmp(pb[i].lastName, lName) == 0)) {
                    printf("%s\t%s\t%s\n\n", pb[i].firstName, pb[i].lastName, pb[i].phoneNumber);
                    isFound = 1;
                    break;                     
                 } 
             }
             
             if (isFound == 0) {
                printf("\n%s %s was not Found.\n\n", fName, lName);
             }
         }
         
    }
         
    void displayRandom() {
         int y = 0;
         system("cls");
         if (count == 0){
                   printf("The phonebook is empty.\n");
                   system("pause");
         } else {
             y = rand()%count;
             printf("%s\t%s\t%s\n\n", pb[y].firstName, pb[y].lastName, pb[y].phoneNumber);
         }
         
    }
         
    void deleteAll() {
         system("cls");
         if (count == 0) {
                   printf("The Phonebook is empty.\n");
                   system("pause");
         }
         free(pb);
         pb = 0;
         count = 0;
         
         if (pb == 0) {
                printf("The Phonebook has been completely deleted.\n");
         }
    }
