/*
 * Author: Sheldon Burks
 * About: CECS 420 Project 1
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MEM_ERROR() printf("Memory Error. Exiting");

typedef struct Node {
  struct Node *next;
  struct Node *prev;
  int pid;
  int arrival_time;
  int burst_time;
  int finished_time;
  int waiting_time;
  int time_remaining;
  int last_time_executed;
  int status;
} Node;

typedef struct List {
  Node *head;
  Node *tail;
} List;

List * create_list()
{
  return calloc(1, sizeof(List));
}

void push(List* list, int pid, int arrival_time, int burst_time, int finished_time, int waiting_time, int time_remaining, int last_time_executed)
{
  Node *node = calloc(1, sizeof(Node));
  if(node == NULL)
  {
    MEM_ERROR();
    exit(-1);
  }
  node->pid = pid;
  node->arrival_time = arrival_time;
  node->burst_time = burst_time;
  node->finished_time = finished_time;
  node->waiting_time = waiting_time;
  node->time_remaining = time_remaining;
  node->last_time_executed = last_time_executed;
  node->status = 0;

  if(list->tail == NULL)
  {
    list->head = node;
    list->tail = node;
  }
  else
  {
    list->tail->next = node;
    node->prev = list->tail;
    list->tail = node;
  }
}

void head_to_tail(List *list)
{
  if(list->head == list->tail)
  {
    return;
  }
  Node *temp = list->head;
  list->head = temp->next;
  list->head->prev = NULL;

  list->tail->next = temp;
  temp->prev = list->tail;
  temp->next = NULL;
  list->tail = temp;
}

void remove_node(List *list, Node *node)
{
  if(list->head == NULL && list->tail == NULL)
  {
    return;
  }
  if(node == NULL)
  {
    return;
  }

  if(node == list->head && node == list->tail)
  {
    list->head = NULL;
    list->tail = NULL;
  }
  else if(node == list->head)
  {
    list->head = node->next;
    list->head->prev = NULL;
  }
  else if(node == list->tail)
  {
    list->tail = node->prev;
    list->tail->next = NULL;
  }
  else
  {
    Node *after = node->next;
    Node *before = node->prev;
    after->prev = before;
    before->next = after;
  }
  free(node);
}

void pop(List *list)
{
  Node *node = list->head;
  remove_node(list, node);
}

void free_list(List *list)
{
  Node* curr = NULL;
  for(curr = list->head; curr != NULL; curr = curr->next)
  {
    if(curr->prev)
    {
      free(curr->prev);
    }
  }

  free(list->tail);
  free(list);
}

int is_empty(List *list)
{
  if(list->tail == NULL)
  {
    return 1;
  }

  return 0;
}

void print_list(List *list)
{
  Node *curr;
  for(curr = list->head; curr != NULL; curr = curr->next)
  {
    printf("PID: %d, Arrival Time: %d, Burst Time: %d\n", curr->pid, curr->arrival_time, curr->burst_time);
  }
}

void read(List* list, char* file_name)
{
  int pid;
  int arrival_time;
  int burst_time;

  FILE *file = fopen(file_name, "r");

  if(file == NULL)
  {
    printf("Unable to open file %s\n", file_name);
    exit(-1);
  }

  while(fscanf(file, "%d %d %d", &pid, &arrival_time, &burst_time) != EOF) {
    push(list, pid, arrival_time, burst_time, 0, 0, burst_time, 0);
  }
  fclose(file);
}

void read_limit(List* list, char* file_name, int limit)
{
  int pid;
  int arrival_time;
  int burst_time;

  FILE *file = fopen(file_name, "r");

  if(file == NULL)
  {
    printf("Unable to open file %s\n", file_name);
    exit(-1);
  }
  int i;
  for(i = 0; i < limit; i++)
  {
    if(fscanf(file, "%d %d %d", &pid, &arrival_time, &burst_time) != EOF)
    {
      push(list, pid, arrival_time, burst_time, 0, 0, burst_time, 0);
    }
  }
  fclose(file);
}

void write_list(char *file_name, List *list)
{
  FILE *file = fopen(file_name, "w");
  Node *curr = NULL;
  for(curr = list->head; curr != NULL; curr = curr->next)
  {
    fprintf(file, "%d %d %d %d\n", curr->pid, curr->arrival_time, curr->finished_time, curr->waiting_time);
  }
 fclose(file);
}

int all_processed(List *list)
{
  Node *curr;
  for(curr = list->head; curr != NULL; curr = curr->next)
  {
    if(curr->status == 0)
    {
      return 0;
    }
  }
  return 1;
}

void shortest_job_first(List *list, char *out_file)
{
  int current_time = 0;
  int waiting_time = 0;
  int finished_time = 0;
  List *ready_queue = create_list();
  List *finished_list = create_list();


  do {
    Node *curr;
    for(curr = list->head; curr != NULL; curr = curr->next)
    {
      if(curr->arrival_time <= current_time && curr->status == 0)
      {
        push(ready_queue, curr->pid, curr->arrival_time, curr->burst_time, 0, 0, curr->time_remaining, 0);
        curr->status = 1;
      }
    }

    if(is_empty(ready_queue))
    {
      current_time++;
      continue;
    }

    Node *min_burst_node = ready_queue->head;
    for(curr = ready_queue->head; curr != NULL; curr = curr->next)
    {
      if(curr->burst_time < min_burst_node->burst_time)
      {
        min_burst_node = curr;
      }
    }

    waiting_time = current_time - min_burst_node->arrival_time;
    current_time += min_burst_node->burst_time;
    finished_time = current_time;

    push(finished_list, min_burst_node->pid, min_burst_node->arrival_time, min_burst_node->burst_time, finished_time, waiting_time, 0, 0);

    remove_node(ready_queue, min_burst_node);

  } while(!is_empty(ready_queue) || !all_processed(list));
  write_list(out_file, finished_list);
  free_list(ready_queue);
  free_list(finished_list);
}

void round_robin(List *list, int quantum, char *out_file)
{
  int current_time = 0;
  int tie_flag = 0;
  int finished_flag = 0;
  List *ready_queue = create_list();
  List *finished_list = create_list();

  do {
    tie_flag = 0;
    finished_flag = 0;

    Node *curr;
    for(curr = list->head; curr != NULL; curr = curr->next)
    {
      if(curr->arrival_time <= current_time && curr->status == 0)
      {
        push(ready_queue, curr->pid, curr->arrival_time, curr->burst_time, 0, current_time-curr->arrival_time, curr->time_remaining, current_time);
        curr->status = 1;
      }
    }

    if(is_empty(ready_queue))
    {
      current_time++;
      continue;
    }

    ready_queue->head->waiting_time += current_time - ready_queue->head->last_time_executed;
    ready_queue->head->time_remaining -= quantum;
    current_time += quantum;

    //Correct the time if the time reamining on the current node goes negative
    if(ready_queue->head->time_remaining < 0)
    {
      current_time += ready_queue->head->time_remaining;
    }

    ready_queue->head->last_time_executed = current_time;

    for(curr = list->head; curr != NULL; curr = curr->next)
    {
      if(curr->arrival_time <= current_time && curr->status == 0)
      {
        push(ready_queue, curr->pid, curr->arrival_time, curr->burst_time, 0, current_time-curr->arrival_time, curr->time_remaining, current_time);
        curr->status = 1;
      }
    }

    if(ready_queue->head->time_remaining <= 0)
    {
      push(finished_list, ready_queue->head->pid, ready_queue->head->arrival_time, ready_queue->head->burst_time, current_time, ready_queue->head->waiting_time, 0, 0);
      pop(ready_queue);
      finished_flag = 1;
    }
    else
    {
      //Look for ties and insert the head of the ready_queue where it needs to be
      for(curr = ready_queue->tail; curr != NULL; curr=curr->prev)
      {
        if(curr->arrival_time == current_time)
        {
          tie_flag = 1;
          Node *temp = ready_queue->head;
          ready_queue->head = temp->next;
          ready_queue->head->prev = NULL;

          temp->prev = curr->prev;
          temp->next = curr;
          curr->prev->next = temp;
          curr->prev = temp;

        }
      }
    }

    //if there are no ties and nothing finished this cycle,
    //then push the head node to the tail.
    if(tie_flag == 0 && finished_flag == 0)
    {
      head_to_tail(ready_queue);
    }

  } while(!is_empty(ready_queue) || !all_processed(list));
  write_list(out_file, finished_list);
  free_list(ready_queue);
  free_list(finished_list);
}

int main(int argc, char *argv[])
{
  int quantum = 0;
  int limit = 0;

  if(argc < 3)
  {
    printf("You must have an input file, output file, and the algorithm to use.\n");
    exit(-1);
  }

  if(strcmp(argv[3], "SJF") == 0 && argv[4] != NULL)
  {
    limit = argv[4] ? atoi(argv[4]) : 0;
  }
  else if(strcmp(argv[3], "RR") == 0)
  {
    quantum = atoi(argv[4]);
    limit = argv[5] ? atoi(argv[5]) : 0;
  }

  List *list = create_list();

  if(limit != 0)
  {
    read_limit(list, argv[1], limit);
  }
  else
  {
    read(list, argv[1]);
  }

  if(strcmp(argv[3], "SJF") == 0)
  {
    shortest_job_first(list, argv[2]);
  }
  else if(strcmp(argv[3], "RR") == 0)
  {
    round_robin(list, quantum, argv[2]);
  }

  free_list(list);
  return 0;
}
