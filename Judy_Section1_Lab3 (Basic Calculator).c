// Basic Calculator
// Author: Alex Judy
// Date: 1/27/2014
// Section: 1

#include <stdio.h>



    void addition(int, int);        //Void these inputs so that the program does not recognize them as a value
    void subtraction(int, int);
    void multiplication(int, int);
    void division(int, int);
    void modulus(int, int);
    void prime(int);


int main()

{
printf("Basic Calculator\n"); //Program Title
int userInput=0; //Default user input
int exit = 7, num1 = 0, num2 = 0 ; //Default values for first number and second number. Exit will always = 7
while(userInput!=7) //Repeat program if user does not select exit (7)

 {
  //Calculator function selection menu
  printf("\n -------------------\n");
  printf("|(1) Addition       |\n");
  printf("|(2) Subtraction    |\n");
  printf("|(3) Multiplication |\n");
  printf("|(4) Division       |\n");
  printf("|(5) Modulus        |\n");
  printf("|(6) Test if Prime  |\n");
  printf("|(7) Exit Calculator|\n");
  printf(" -------------------\n");
  printf("\nPlease Select Operation: \n"); //Select Operation

   {scanf("%d", &userInput); //User operation choice

   switch(userInput)

    { case 1: case 2: case 3: case 4: case 5: //Options 1-5
      printf("Enter your first number: \n");
		scanf("%d" , &num1);
      printf("Enter your second number: \n");
		scanf("%d" , &num2);
		switch(userInput)

	   { case 1: //Option 1 for addition
		addition (num1, num2);
		break;

		 case 2: //Option 2 for subtraction
		subtraction (num1, num2);
	 	break;

		 case 3: //Option 3 for multiplication
		multiplication (num1, num2);
		break;

		 case 4: //Option 4 for division
		division (num1, num2);
		break;

		 case 5: //Option 5 for modulus
		modulus (num1, num2);
		break;
       }
        break;

     case 6: //Option 6 for prime test
       	printf("Enter Number: \n");
       	scanf("%d", &num1);
       	switch(userInput)
       {
         case 6:
	   	prime (num1);
       	break;
       }

   	    break;

	    case 7: //Exit option
         printf("Good Bye!\n");
         exit = 7;
         break;

	   default:	//If a number less than 1 and greater than 7 is entered it is invalid
	     printf("Incorrect input. Please enter a valid input");
         break;

    } //End calculator functions
   } //End user input for operations and switch
  } //End calculator
return 0;
system("pause");
} //End Main


//Definines what each user input operation does
       void addition(int n1, int n2) { //Function for addition
            printf("%d + %d = %d", n1, n2, n1 + n2);
            }

       void subtraction(int n1, int n2) { //Function for subtraction
            printf("%d - %d = %d", n1, n2, n1 - n2);
            }

       void multiplication(int n1, int n2) { //Function for multplication
            printf("%d * %d = %d", n1, n2, n1 * n2);
            }

       void division(int n1, int n2) { //Function for division
            printf("%d / %d = %d", n1, n2, n1 / n2);
            }

       void modulus(int n1, int n2) { //Function to show the remainder after division
            printf("%d %% %d = %d", n1, n2, n1 % n2);
            }

       void prime(int n) {
            int prime = 1, test = 2; // Use default variables for test
            while(prime && test < n && n > 1)  // While a number is still known to be prime, greater than one and the test number is lower than it:
                            {
                             if ((n % test) == 0) // //Check for prime
                             prime = 0; // Prime = 0 which exits loop
                              else
                             test += 1; // Increase number and test as a factor
                            }
                               if (!prime){ // If not prime:
                               printf("Not prime: %d * %d = %d", test, (n / test), n); //If not prime then print factors of number
                                          }
                                             else {
                                             printf("Prime!"); //If prime then print prime
                                                  }
                        }